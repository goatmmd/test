from telethon import TelegramClient
from telethon.tl.functions.channels import EditBannedRequest
from telethon.tl.types import ChatBannedRights
import asyncio
import datetime

api_id = 15890646  # Your API_ID
api_hash = "927d43de2718eaafbc0e713cfc8c8c49"  # Your APP_ID
group = input("Enter the group username where the script should search for deleted accounts: ")

with TelegramClient("deleteacc", api_id, api_hash) as client:
    with client:
        for dialog in client.iter_dialogs():
            if dialog.is_channel:
                if dialog.name == group:
                    channel_id = dialog.id
                    break


async def clear_chat(client):
    deleted_accounts = 0
    async for user in client.iter_participants(group):
        if user.deleted:
            try:
                deleted_accounts += 1
                await client(EditBannedRequest(group, user, ChatBannedRights(
                    until_date=datetime.timedelta(minutes=1),
                    view_messages=True
                )))
            except Exception as exc:
                print(f"Failed to kick one deleted account because: {str(exc)}")
    if deleted_accounts:
        print(f"Kicked {deleted_accounts} Deleted Accounts")
    else:
        print(f"No deleted accounts found in {group}")


with TelegramClient("anon", api_id, api_hash) as client:
    asyncio.get_event_loop().run_until_complete(clear_chat(client))
